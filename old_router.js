
// 전체 메모 리스트 요약
app.get('/memos', (req, res, next) => {
    const memoList = memos.map((memo) => {
        const { author, title } = memo;
        return { author, title};
    })

    if (memoList.length < 1) {
        next(new Error('There is no such memo with the id.'));
    }
    res.send(memoList);
})

// 유저별, 상세가 겹치면 id는 작동은 안된다. 동작이 같아서 인것은 알겠늗네 그러면 어떻게 해야할까
// 유저별 메모 리스트 /:author
// app.get('/memos/:author', (req, res) => {
//     const { author } = req.params;
//     const memosByAuthor = memos.filter((memo) => memo.author === author);
    
//     res.json(memosByAuthor);
// })

// 상세 메모 /:id
app.get('/memos/:id', (req, res, next) => {
    const { id } = req.params;
    const memoById = memos.filter((memo) => memo.id == id);

    if (memoById.length < 1) {
        next(new Error('There is no such memo with the id.'));
    }

    res.send(memoById);
})

// 메모 생성 /:memo
app.post('/memos', (req, res, next) => {
    const newId = memos[memos.length-1].id + 1;
    const { author, title, content } = req.body;
    
    memos.push({
        id: newId, author, title, content
    });
    res.send('New Memo Created!');
})

// 메모 업데이트 /:id
app.put('/memos/:id', (req, res, next) => {
    const { id } = req.params;
    const { title, content } = req.body;

    const idx = memos.findIndex((memo) => memo.id == id);

    if (idx < 0) {
        next(new Error('There is no such memo with the id.'));
    }

    const memo = memos[idx];
    memo.title = title;
    memo.content = content;

    res.send('Updated !');
})

// 메모 삭제 /:memos/:id
app.delete('/memos/:id', (req, res, next) => {
    const { id } = req.params;
    const idx = memos.findIndex((memo) => memo.id == id);

    if (idx < 0) {
        next(new Error('There is no such memo with the id.'));
    }
    memos.splice(idx, 1);
})