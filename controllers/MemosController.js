const { Memo } = require('../models');

exports.createMemo = async (req, res) => {
  const { author, title, content } = req.body;
  const newMemo = await Memo.create({ author, title, content });
  res.send(newMemo);
};

exports.getAllMemos = async (req, res) => {
  const allMemos = await Memo.find();
  res.send(allMemos);
}

exports.getMemosByAuthor = async (req, res) => {
  const { author } = req.params;
  const memosByAuthor = await Memo.find({ author });
  res.send(memosByAuthor);
}

exports.updateMemo = async (req, res) => {
  const { author } = req.params;
  const { title, content } = req.body;

  const updateFields= {};
  
  updateFields.title = title ?? updateFields.title;
  updateFields.content = content ?? updateFields.content;


  const updatedMemo = await Memo.findOneAndUpdate({ author }, updateFields, {new : true});
  res.send(updatedMemo);
}

exports.deleteMemo = async (req, res) => {
  const { author } = req.params;
  const result = await Memo.deleteOne({ author });
  res.send(`${result} row deleted`);
}