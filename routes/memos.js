const { Router } = require('express');
const router = Router();
const memosController = require('../controllers/MemosController');

// 전체 메모 리스트 불러오기
router.get('/', memosController.getAllMemos);

// 상세 메모 확인하기
router.get('/:author', memosController.getMemosByAuthor);


// 메모 생성하기
router.post('/', memosController.createMemo)

// 메모 수정하기
router.put('/:author', memosController.updateMemo)


// 메모 삭제하기
router.delete('/:author', memosController.deleteMemo)

module.exports = router;