const mongoose = require('mongoose');
const { Schema } = require('mongoose');

const MemoSchema = new Schema({
  _id: { type: mongoose.Types.ObjectId, auto: true },
  author: String,
  title: String,
  content: String,
}, {
  timestamps: true,
});

module.exports = MemoSchema;