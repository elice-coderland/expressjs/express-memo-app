const express = require('express');
const mongoose = require('mongoose');

const app = express();

const bodyParser = require('body-parser');
const memoRouter = require('./routes/memos');

mongoose.connect('')


app.use(bodyParser.json());

// entry point
app.get('/', (req, res) => {
    res.send('Hello, World!');
});

app.use('/memos', memoRouter)

// error handler - 제대로 작동하지 않음
app.use((err, req, res, next) => {
   res.send(err)
})

app.listen(8080, () => {
    console.log(`'Hello, Memo' Server is running...!`);
})